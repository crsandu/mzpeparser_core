#pragma once
#include <Windows.h>
#include "jsoncons/json.hpp"
#include "ParseFile.hpp"

#define noValidation 0x1    /* the section will not be validated */

class HeaderAnalysis{
public:
    static jsoncons::json getDosHeader(PAlyStruct data);
    static jsoncons::json getFileHeader(PAlyStruct data);
    
    static jsoncons::json getNtHeader32(PAlyStruct data);
    static jsoncons::json getNtHeader64(PAlyStruct data);

    static jsoncons::json getOptionalHeader32(PAlyStruct data);
    static jsoncons::json getOptionalHeader64(PAlyStruct data);

    static jsoncons::json getImageSection(PAlyStruct data);
};