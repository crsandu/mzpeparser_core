#include "HeaderVerf.hpp"

bool 
HeaderVerf::isValidMZPE(
    PAlyStruct data
)
{
    data->imageDosHeader = (PIMAGE_DOS_HEADER)data->baseAddr;
    if (!isValidDosHeader(data)) {
        return false;
    }

    /* complete IMAGE_NT_HEADERS structure */
    data->imageNtHeader32 = (PIMAGE_NT_HEADERS32)((PBYTE)data->imageDosHeader + data->imageDosHeader->e_lfanew);
    data->imageNtHeader64 = (PIMAGE_NT_HEADERS64)((PBYTE)data->imageDosHeader + data->imageDosHeader->e_lfanew);
    if (!isValidNtHeader(data)) {
        return false;
    }

    /* complete IMAGE_FILE_HEADERS */
    data->imageFileHeader = (PIMAGE_FILE_HEADER)&data->imageNtHeader32->FileHeader;
    if (!isValidFileHeader(data)) {
        return false;
    }

    /* DLL file */
    data->isDLL = data->imageFileHeader->Characteristics & IMAGE_FILE_DLL;

    /* number of sections */
    data->NumberOfSections = data->imageFileHeader->NumberOfSections;

    /* completam datele legate de arhitectura masinii */
    data->arhitectura = data->imageNtHeader32->FileHeader.Machine == IMAGE_FILE_MACHINE_I386 ? 32 : 64; // deja au fost validate datele

    if (data->arhitectura == 32) {
        data->imageOptionalHeader32 = &data->imageNtHeader32->OptionalHeader;
    }
    else {
        data->imageOptionalHeader64 = &data->imageNtHeader64->OptionalHeader;
    }

    /* complete IMAGE_OPTIONAL_HEADERS(x32/x64) */
    if (!isValidOptionalHeader(data)) {
        return false;
    }

    /* Alignment  settings save */
    if (data->arhitectura == 32) {
        data->FileAlignment = data->imageOptionalHeader32->FileAlignment;
        data->SectionAlignment = data->imageOptionalHeader32->SectionAlignment;
        data->ImageBase = data->imageOptionalHeader32->ImageBase;
    }
    else {
        data->FileAlignment = data->imageOptionalHeader64->FileAlignment;
        data->SectionAlignment = data->imageOptionalHeader64->SectionAlignment;
        data->ImageBase = data->imageOptionalHeader64->ImageBase;
    }

    /* complete IMAGE_SECTION_HEADERS */
    data->imageSectionsHeaders = data->arhitectura == 32 ? \
        (PIMAGE_SECTION_HEADER)(((PBYTE)data->imageNtHeader32) + sizeof(IMAGE_NT_HEADERS32)) : \
        (PIMAGE_SECTION_HEADER)(((PBYTE)data->imageNtHeader64) + sizeof(IMAGE_NT_HEADERS64));
    if (!isValidSectionHeaders(data)) {
        return false;
    }

    return true;
}

bool
HeaderVerf::isValidDosHeader(
    PAlyStruct data
)
/*
# Parametrii:
    PBYTe mapView = pointer la zona unde este fisierul mapat in memorie
    PAlyStruct data = pointer la o zona de date comune, este facut pentru accelerarea procesului de analiza
# Detalii :
    MSDN : https ://docs.microsoft.com/en-us/windows/win32/debug/pe-format#file-headers
    Info : http ://stixproject.github.io/data-model/1.2/WinExecutableFileObj/DOSHeaderType/
    Analiza IMAGE_DOS_HEADER
# Return:
    true if IMAGE_DOS_HEADER is valid else returns false
*/
{
    PIMAGE_DOS_HEADER imageHeader = data->imageDosHeader;

    if (data->fileSize < sizeof(IMAGE_DOS_HEADER)) {
        return false;
    }

    /* e_magic must be MZ == 0x4d5a */
    if (imageHeader->e_magic != IMAGE_DOS_SIGNATURE) {
        return false;
    }

    return true;
}

bool
HeaderVerf::isValidNtHeader(
    PAlyStruct data
)
/*
# Parametrii:
    PBYTe mapView = pointer la zona unde este fisierul mapat in memorie
    PAlyStruct data = pointer la o zona de date comune, este facut pentru accelerarea procesului de analiza
# Detalii :
    MSDN : https ://docs.microsoft.com/en-us/windows/win32/debug/pe-format#file-headers
    Analiza IMAGE_DOS_HEADER
# Return:
    true if IMAGE_NT_HEADERS is valid else returns false
*/
{
    PIMAGE_NT_HEADERS32 imageNTHeader32 = data->imageNtHeader32;

    /* sizeof(Signature) + sizeof(IMAGE_FILE_HEADER) */
    if (data->fileSize < sizeof(IMAGE_DOS_HEADER) + \
        + sizeof(DWORD)  + sizeof(IMAGE_FILE_HEADER)) {
        return false;
    }

    /* e_magic must be Signature == 'PE00' */
    if (imageNTHeader32->Signature != IMAGE_NT_SIGNATURE) {
        return false;
    }

    return true;
}

bool 
HeaderVerf::isValidFileHeader(
    PAlyStruct data
) {
    PIMAGE_FILE_HEADER fileHeader = data->imageFileHeader;
    
    /* verificam arhitectura masinii */
    if (fileHeader->Machine != IMAGE_FILE_MACHINE_I386 && \
        fileHeader->Machine != IMAGE_FILE_MACHINE_IA64 && \
        fileHeader->Machine != IMAGE_FILE_MACHINE_AMD64) {
        return false;
    }

    if (fileHeader->NumberOfSections > 96) {
        return false;
    }
    return true;
}

bool
HeaderVerf::isValidOptionalHeader(
    PAlyStruct data
) {
    PIMAGE_OPTIONAL_HEADER64 imgOptional64 = data->imageOptionalHeader64;
    PIMAGE_OPTIONAL_HEADER32 imgOptional32 = data->imageOptionalHeader32;

    DWORD64 sizeImages = sizeof(IMAGE_DOS_HEADER) + sizeof(IMAGE_FILE_HEADER) + sizeof(DWORD);

    if (data->arhitectura == 64) {
        goto Analysis64bit;
    }

    /* analiza pe 32 de biti */
    if (sizeImages + sizeof(IMAGE_OPTIONAL_HEADER32) > data->fileSize) {
        return false;
    }

    if (imgOptional32->Magic != IMAGE_NT_OPTIONAL_HDR32_MAGIC) {
        return false;
    }

    if ((DWORD64)imgOptional32->SizeOfInitializedData + imgOptional32->SizeOfUninitializedData  > data->fileSize) {
        return false;
    }

    /* check if the file is DLL, then entry point can be 0 */
    if (!(imgOptional32->AddressOfEntryPoint == 0 && data->isDLL) &&
        (imgOptional32->AddressOfEntryPoint > data->fileSize)) {
        return false;
    }

    /* value is a multiple of 64K bytes. The default value for DLLs is 0x10000000. */
    if ((data->isDLL && imgOptional32->ImageBase != 0x10000000) || 
        imgOptional32->ImageBase % (2 << 16) != 0) {
        return false;
    }

    /* This value must be greater than or equal to the FileAlignment member. */
    if (imgOptional32->SectionAlignment < imgOptional32->FileAlignment) {
        return false;
    }

    /* This member is reserved and must be 0. */
    if (imgOptional32->Win32VersionValue != 0) {
        return false;
    }

    /* the size of the image, in bytes, including all headers. Must be a multiple of SectionAlignment. */
    if (imgOptional32->SizeOfImage % imgOptional32->SectionAlignment != 0) {
        return false;
    }

    if (imgOptional32->SizeOfHeaders > data->fileSize) {
        return false;
    }

    if (imgOptional32->Subsystem > 16) {
        return false;
    }

    return true;

Analysis64bit:
    /* analiza pe 64 de biti */
    if (sizeImages + sizeof(IMAGE_OPTIONAL_HEADER64) > data->fileSize) {
        return false;
    }

    if (imgOptional64->Magic != IMAGE_NT_OPTIONAL_HDR64_MAGIC) {
        return false;
    }

    if ((DWORD64)imgOptional64->SizeOfInitializedData + imgOptional64->SizeOfUninitializedData > data->fileSize) {
        return false;
    }

    /* check if the file is DLL, then entry point can be 0 */
    if (!(imgOptional64->AddressOfEntryPoint == 0 && data->isDLL) &&
        (imgOptional64->AddressOfEntryPoint > data->fileSize)) {
        return false;
    }

    if (imgOptional64->ImageBase % (2 << 16) != 0) {
        return false;
    }

    /* This value must be greater than or equal to the FileAlignment member. */
    if (imgOptional64->SectionAlignment < imgOptional64->FileAlignment) {
        return false;
    }

    /* This member is reserved and must be 0. */
    if (imgOptional64->Win32VersionValue != 0) {
        return false;
    }

    /* the size of the image, in bytes, including all headers. Must be a multiple of SectionAlignment. */
    if (imgOptional64->SizeOfImage % imgOptional64->SectionAlignment != 0) {
        return false;
    }

    if (imgOptional64->SizeOfHeaders > data->fileSize) {
        return false;
    }

    if (imgOptional64->Subsystem > 16) {
        return false;
    }

    return true;
}

bool
HeaderVerf::isValidSectionHeaders(
    PAlyStruct data
) {
    PIMAGE_SECTION_HEADER imageSection = NULL;

    /* verificam daca structurile sunt in interiorul fisierului */
    DWORD64 size = sizeof(IMAGE_DOS_HEADER);
    size += data->arhitectura == 32 ? sizeof(IMAGE_NT_HEADERS32) : sizeof(IMAGE_NT_HEADERS64);

    std::cout << "Number of sections: " << data->NumberOfSections << std::endl;
    
    for (DWORD count = 0; count < data->NumberOfSections; count++) {
        imageSection = data->imageSectionsHeaders + count;

        /* verificam daca structura se afla in fisier */
        if ((DWORD64)imageSection->PointerToRawData + imageSection->SizeOfRawData > data->fileSize) {
            return false;
        }

        /* value must be a multiple of the FileAlignment */
        if (imageSection->SizeOfRawData % data->FileAlignment != 0) {
            return false;
        }

        /* value must be a multiple of the FileAlignment */
        if (imageSection->PointerToRawData % data->FileAlignment != 0) {
            return false;
        }
    }

    return true;
}