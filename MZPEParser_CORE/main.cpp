#include <Windows.h>
#include <string>

#include "ParseFile.hpp"
#include "HeadersAnalysis.hpp"
#include "HeaderVerf.hpp"

typedef struct _UNICODE_STRING64{
    LPSTR Buffer;
    DWORD64 Length;
}UNICODE_STRING64, * PUNICODE_STRING64;

const char* errMsg = "Error";

extern "C"
__declspec(dllexport)
UNICODE_STRING64
parseMZ(
    LPCWSTR fileName
) {
    std::string dataObj;
    ParseFile fileData = ParseFile(fileName);
    UNICODE_STRING64 returnMessage = { 0 };

    /* nu s-a putut crea obiectul */
    if(fileData.lastError != 0){
        returnMessage.Buffer = (PCHAR)fileName;
        return returnMessage;
    }

    /* pointer-ul este invalid */
    if (!fileData.verifyContent()) {
        return returnMessage;
    }

    dataObj = fileData.getStringFormat();
    returnMessage.Length = dataObj.size();
    returnMessage.Buffer = new char[returnMessage.Length + 1];

    strncpy_s(returnMessage.Buffer, returnMessage.Length + 1, dataObj.c_str(), returnMessage.Length);

    return returnMessage;
}

BOOL 
APIENTRY 
DllMain( 
    HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
) {
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

