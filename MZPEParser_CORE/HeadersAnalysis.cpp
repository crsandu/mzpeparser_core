#include <string.h>

#include "HeadersAnalysis.hpp"
#include "HeaderVerf.hpp"

jsoncons::json
HeaderAnalysis::getDosHeader(
    PAlyStruct data
)
/*
+++
# Parametrii:
    PBYTe mapView = pointer la zona unde este fisierul mapat in memorie
    DOWRD options = optiunea cu care sa se executa parser-ul
    PAlyStruct data = pointer la o zona de date comune, este facut pentru accelerarea procesului de analiza
# Detalii:
    MSDN: https://docs.microsoft.com/en-us/windows/win32/debug/pe-format#file-headers
    Info: http://stixproject.github.io/data-model/1.2/WinExecutableFileObj/DOSHeaderType/
    Analiza IMAGE_DOS_HEADER
# Returneaza:
    -> new jsoncons::json cu datele din IMAGE_DOS_HEADER sau nullptr daca functia nu a functionat cu success
---
*/
{
    PIMAGE_DOS_HEADER imageHeader = data->imageDosHeader;

    return jsoncons::json::object{
        { "e_magic", imageHeader->e_magic },
        { "e_cblp", imageHeader->e_cblp },
        { "e_cp", imageHeader->e_cblp },
        { "e_crlc", imageHeader->e_crlc },
        { "e_cparhdr", imageHeader->e_cparhdr },
        { "e_minalloc", imageHeader->e_minalloc },
        { "e_maxalloc", imageHeader->e_maxalloc },
        { "e_ss", imageHeader->e_ss },
        { "e_sp", imageHeader->e_sp },
        { "e_csum", imageHeader->e_csum },
        { "e_ip", imageHeader->e_ip },
        { "e_cs", imageHeader->e_cs },
        { "e_lfarlc", imageHeader->e_lfarlc },
        { "e_ovno", imageHeader->e_ovno },
        // { "e_res", imageHeader->e_res }, -- Disabled
        { "e_oemid", imageHeader->e_oemid },
        { "e_oeminfo", imageHeader->e_oeminfo },
        // { "e_res2", imageHeader->e_res2 }, -- Disabled
        { "e_lfanew", imageHeader->e_lfanew },
    };
}

jsoncons::json 
HeaderAnalysis::getNtHeader64(
    PAlyStruct data
)
{
    PIMAGE_NT_HEADERS64 imageHeader32 = data->imageNtHeader64;
    jsoncons::json iHValue = jsoncons::json();

    iHValue["Signature"] = imageHeader32->Signature;
    iHValue["IMAGE_FILE_HEADER"] = HeaderAnalysis::getFileHeader(data);
    iHValue["IMAGE_OPTIONAL_HEADERS"] = HeaderAnalysis::getOptionalHeader64(data);

   
    return iHValue;
}

jsoncons::json
HeaderAnalysis::getNtHeader32(
    PAlyStruct data
)
{
    PIMAGE_NT_HEADERS32 imageHeader32 = data->imageNtHeader32;
    jsoncons::json iHValue = jsoncons::json();

    iHValue["Signature"] = imageHeader32->Signature;
    iHValue["IMAGE_FILE_HEADER"] = HeaderAnalysis::getFileHeader(data);
    iHValue["IMAGE_OPTIONAL_HEADERS"] = HeaderAnalysis::getOptionalHeader32(data);

    return iHValue;
}

jsoncons::json
HeaderAnalysis::getFileHeader(
    PAlyStruct data
) {
    PIMAGE_FILE_HEADER imageFileHeader = data->imageFileHeader;

    return jsoncons::json::object{
        { "Machine", imageFileHeader->Machine == IMAGE_FILE_MACHINE_I386 ? "Intel Itanium" : "x64" },
        { "NumberOfSections", imageFileHeader->NumberOfSections },
        { "TimeDateStamp", imageFileHeader->TimeDateStamp },
        { "PointerToSymbolTable", imageFileHeader->PointerToSymbolTable },
        { "NumberOfSymbols", imageFileHeader->NumberOfSymbols },
        { "SizeOfOptionalHeader", imageFileHeader->SizeOfOptionalHeader },
        { "Characteristics", imageFileHeader->Characteristics }
    };
}

jsoncons::json
HeaderAnalysis::getOptionalHeader64(
    PAlyStruct data
) {
    PIMAGE_OPTIONAL_HEADER64 imageOptionalHeader = data->imageOptionalHeader64;

    return jsoncons::json::object{
        { "Magic", imageOptionalHeader->Magic },
        { "MajorLinkerVersion", imageOptionalHeader->MajorLinkerVersion },
        { "MinorLinkerVersion", imageOptionalHeader->MinorLinkerVersion },
        { "SizeOfCode", imageOptionalHeader->SizeOfCode },
        { "SizeOfInitializedData", imageOptionalHeader->SizeOfInitializedData },
        { "SizeOfUninitializedData", imageOptionalHeader->SizeOfUninitializedData },
        { "AddressOfEntryPoint", imageOptionalHeader->AddressOfEntryPoint },
        { "BaseOfCode", imageOptionalHeader->BaseOfCode },
        { "ImageBase", imageOptionalHeader->ImageBase },
        { "SectionAlignment", imageOptionalHeader->SectionAlignment },
        { "FileAlignment", imageOptionalHeader->FileAlignment },
        { "MajorOperatingSystemVersion", imageOptionalHeader->MajorOperatingSystemVersion },
        { "MinorOperatingSystemVersion", imageOptionalHeader->MinorOperatingSystemVersion },
        { "MajorImageVersion", imageOptionalHeader->MajorImageVersion },
        { "MajorSubsystemVersion", imageOptionalHeader->MajorSubsystemVersion },
        { "MinorSubsystemVersion", imageOptionalHeader->MinorSubsystemVersion },
        { "Win32VersionValue", imageOptionalHeader->Win32VersionValue },
        { "SizeOfImage", imageOptionalHeader->SizeOfImage },
        { "SizeOfHeaders", imageOptionalHeader->SizeOfHeaders },
        { "CheckSum", imageOptionalHeader->CheckSum },
        { "Subsystem", imageOptionalHeader->Subsystem },
        { "DllCharacteristics", imageOptionalHeader->DllCharacteristics },
        { "SizeOfStackReserve", imageOptionalHeader->SizeOfStackReserve },
        { "SizeOfStackCommit", imageOptionalHeader->SizeOfStackCommit },
        { "SizeOfHeapReserve", imageOptionalHeader->SizeOfHeapReserve },
        { "SizeOfHeapCommit", imageOptionalHeader->SizeOfHeapCommit },
        { "LoaderFlags", imageOptionalHeader->LoaderFlags },
        { "NumberOfRvaAndSizes", imageOptionalHeader->NumberOfRvaAndSizes }
    };
}

jsoncons::json
HeaderAnalysis::getOptionalHeader32(
    PAlyStruct data
) {
    PIMAGE_OPTIONAL_HEADER32 imageOptionalHeader = data->imageOptionalHeader32;

    return jsoncons::json::object{
        { "Magic", imageOptionalHeader->Magic },
        { "MajorLinkerVersion", imageOptionalHeader->MajorLinkerVersion },
        { "MinorLinkerVersion", imageOptionalHeader->MinorLinkerVersion },
        { "SizeOfCode", imageOptionalHeader->SizeOfCode },
        { "SizeOfInitializedData", imageOptionalHeader->SizeOfInitializedData },
        { "SizeOfUninitializedData", imageOptionalHeader->SizeOfUninitializedData },
        { "AddressOfEntryPoint", imageOptionalHeader->AddressOfEntryPoint },
        { "BaseOfCode", imageOptionalHeader->BaseOfCode },
        { "ImageBase", imageOptionalHeader->ImageBase },
        { "SectionAlignment", imageOptionalHeader->SectionAlignment },
        { "FileAlignment", imageOptionalHeader->FileAlignment },
        { "MajorOperatingSystemVersion", imageOptionalHeader->MajorOperatingSystemVersion },
        { "MinorOperatingSystemVersion", imageOptionalHeader->MinorOperatingSystemVersion },
        { "MajorImageVersion", imageOptionalHeader->MajorImageVersion },
        { "MajorSubsystemVersion", imageOptionalHeader->MajorSubsystemVersion },
        { "MinorSubsystemVersion", imageOptionalHeader->MinorSubsystemVersion },
        { "Win32VersionValue", imageOptionalHeader->Win32VersionValue },
        { "SizeOfImage", imageOptionalHeader->SizeOfImage },
        { "SizeOfHeaders", imageOptionalHeader->SizeOfHeaders },
        { "CheckSum", imageOptionalHeader->CheckSum },
        { "Subsystem", imageOptionalHeader->Subsystem },
        { "DllCharacteristics", imageOptionalHeader->DllCharacteristics },
        { "SizeOfStackReserve", imageOptionalHeader->SizeOfStackReserve },
        { "SizeOfStackCommit", imageOptionalHeader->SizeOfStackCommit },
        { "SizeOfHeapReserve", imageOptionalHeader->SizeOfHeapReserve },
        { "SizeOfHeapCommit", imageOptionalHeader->SizeOfHeapCommit },
        { "LoaderFlags", imageOptionalHeader->LoaderFlags },
        { "NumberOfRvaAndSizes", imageOptionalHeader->NumberOfRvaAndSizes }
    };
}

jsoncons::json
HeaderAnalysis::getImageSection(
    PAlyStruct data
) {
    PIMAGE_SECTION_HEADER imageSectionHeader = data->imageSectionsHeaders;

    jsoncons::json dataObj;
    jsoncons::json tempObj;

    CHAR imageName[9] = { 0 };
    CHAR numChr[8] = { 0 };

    for (DWORD i = 0; i < data->NumberOfSections; i++) {
        _itoa_s(i, numChr, 8, 10);
        strncpy_s(imageName, (const char *)imageSectionHeader[i].Name, 8);

        tempObj = jsoncons::json::object{
            { "Name", (PCHAR)imageName },
            { "VirtualAddress", imageSectionHeader[i].VirtualAddress },
            { "VirtualSize", imageSectionHeader[i].Misc.VirtualSize },
            { "SizeOfRawData", imageSectionHeader[i].SizeOfRawData },
            { "PointerToRawData", imageSectionHeader[i].PointerToRawData },
            { "PointerToRelocations", imageSectionHeader[i].PointerToRelocations },
            { "PointerToLinenumbers", imageSectionHeader[i].PointerToLinenumbers },
            { "NumberOfRelocations", imageSectionHeader[i].NumberOfRelocations },
            { "NumberOfLinenumbers", imageSectionHeader[i].NumberOfLinenumbers },
            { "Characteristics", imageSectionHeader[i].Characteristics }
        };
        dataObj.insert_or_assign(numChr, tempObj);
    }

    return dataObj;
}