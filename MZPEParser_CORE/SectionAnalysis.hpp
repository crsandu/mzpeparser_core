#pragma once
#include <Windows.h>
#include "jsoncons/json.hpp"
#include "ParseFile.hpp"

class SectionAnalysis {
public:
    static jsoncons::json getExportDirectory(PAlyStruct data);
    static jsoncons::json getThunkData32(PIMAGE_THUNK_DATA32 thunkData, PAlyStruct data);
    static jsoncons::json getThunkData64(PIMAGE_THUNK_DATA64 thunkData, PAlyStruct data);
    static jsoncons::json getImportDescriptor(PAlyStruct data);
    static DWORD64 RVAtoFA(DWORD64 addrInput, PAlyStruct data);
};