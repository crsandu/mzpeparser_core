#pragma once
#include <Windows.h>
#include "HeadersAnalysis.hpp"
#include "ParseFile.hpp"

class SectionVerf {
public:
    static bool isValidMZPE(PAlyStruct data);
private:
    static bool isValidExportDirectory(PAlyStruct data);
    static bool isValidThunkData64(PIMAGE_THUNK_DATA64 thunkData, PAlyStruct data);
    static bool isValidThunkData32(PIMAGE_THUNK_DATA32 thunkData, PAlyStruct data);
    static bool isValidImportDescriptor(PAlyStruct data);
};