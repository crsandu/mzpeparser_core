#include "SectionAnalysis.hpp"

DWORD64 SectionAnalysis::RVAtoFA(DWORD64 addrInput, PAlyStruct data)
{
    DWORD64 addrOutput = 0;
    PIMAGE_SECTION_HEADER imageSection = nullptr;

    for (DWORD64 i = 0; i < data->NumberOfSections; i++) {
        imageSection = &data->imageSectionsHeaders[i];
        if (addrInput >= imageSection->VirtualAddress && \
            addrInput <= (DWORD64)imageSection->VirtualAddress + imageSection->Misc.VirtualSize) {
            addrOutput = addrInput - imageSection->VirtualAddress + imageSection->PointerToRawData;

            return addrOutput;
        }
    }

    return addrOutput;
}

jsoncons::json
SectionAnalysis::getExportDirectory(
    PAlyStruct data
) {
    PIMAGE_EXPORT_DIRECTORY exportDirectory = data->exportDirectory;
    CHAR chrNum[8] = { 0 };

    PWORD nameOrdinals = data->AddressOfNameOrdinals;
    PDWORD addressOfNames = data->AddressOfNames;
    PDWORD addresOfFunction = data->AddressOfFunctions;

    PCHAR addrName = nullptr;
    WORD ordinalValue = 0;
    DWORD64 addrFunction = 0;

    jsoncons::json tempObj;

    jsoncons::json dataObj = jsoncons::json::object{
        { "Characteristics", exportDirectory->Characteristics },
        { "TimeDateStamp", exportDirectory->TimeDateStamp },
        { "MajorVersion", exportDirectory->MajorVersion },
        { "MinorVersion", exportDirectory->MinorVersion },
        { "Name", exportDirectory->Name },
        { "Base", exportDirectory->Base },
        { "NumberOfFunctions", exportDirectory->NumberOfFunctions },
        { "NumberOfNames", exportDirectory->NumberOfNames },
        { "AddressOfFunctions", exportDirectory->AddressOfFunctions },
        { "AddressOfNames", exportDirectory->AddressOfNames },
        { "AddressOfNameOrdinals", exportDirectory->AddressOfNameOrdinals }
    };
   
    for (DWORD64 count = 0; count < data->exportDirectory->NumberOfFunctions; count++) {
        _itoa_s(count, chrNum, 10);

        ordinalValue = nameOrdinals[count];

        if (ordinalValue > data->exportDirectory->NumberOfNames) {
            tempObj = jsoncons::json::object{
                { "Ordinal",  ordinalValue },
                { "Name", "-" },
                { "Address of function", addresOfFunction[ordinalValue]}
            };
        }
        else {
            tempObj = jsoncons::json::object{
                { "Ordinal",  ordinalValue },
                { "Name", (PCHAR)&data->baseAddr[SectionAnalysis::RVAtoFA(addressOfNames[ordinalValue], data)] },
                { "Address of function", addresOfFunction[ordinalValue]}
            };
        }

        dataObj["Address of Function"][chrNum] = tempObj;
    }

    return dataObj;
}

jsoncons::json
SectionAnalysis::getThunkData32(
    PIMAGE_THUNK_DATA32 thunkData,
    PAlyStruct data
)
{
    PIMAGE_IMPORT_BY_NAME imageData = nullptr;
    DWORD64 addr = NULL;
    DWORD64 count = 0;
    CHAR chrNum[8] = { 0 };

    jsoncons::json dataObj;
    jsoncons::json tempObj;

    do {
        _itoa_s(count++, chrNum, 10);

        /* import by name */
        if (!(thunkData->u1.AddressOfData & ((DWORD64)1 << 63))) {
            addr = SectionAnalysis::RVAtoFA(thunkData->u1.AddressOfData, data);
            imageData = (PIMAGE_IMPORT_BY_NAME)&data->baseAddr[addr];
            
            tempObj = jsoncons::json::object{
                { "Hint", imageData->Hint },
                { "Name", (PCHAR)imageData->Name },
            };
        }

        dataObj[chrNum] = jsoncons::json::object{
            { "AddressOfData",  thunkData->u1.AddressOfData },
            { "isOrdinal", thunkData->u1.Ordinal & (1 << 31) ? 1 : 0 }
        };

        dataObj[chrNum]["IMAGE_IMPORT_BY_NAME"] = tempObj;

        thunkData++;
    } while (thunkData->u1.AddressOfData != 0);

    return dataObj;
}

jsoncons::json
SectionAnalysis::getThunkData64(
    PIMAGE_THUNK_DATA64 thunkData,
    PAlyStruct data
)
{
    PIMAGE_IMPORT_BY_NAME imageData = nullptr;
    DWORD64 addr = NULL;
    DWORD64 count = 0;
    CHAR chrNum[8] = { 0 };

    jsoncons::json dataObj;
    jsoncons::json tempObj;

    do {
        _itoa_s(count++, chrNum, 10);

        /* import by name */
        if (!(thunkData->u1.AddressOfData & ((DWORD64)1 << 63))) {
            addr = SectionAnalysis::RVAtoFA(thunkData->u1.AddressOfData, data);
            imageData = (PIMAGE_IMPORT_BY_NAME)&data->baseAddr[addr];

            tempObj = jsoncons::json::object{
                { "Hint", imageData->Hint },
                { "Name", (PCHAR)imageData->Name },
            };
        }

        dataObj[chrNum] = jsoncons::json::object{
            { "AddressOfData",  thunkData->u1.AddressOfData },
            { "isOrdinal", thunkData->u1.Ordinal & (1 << 63) ? 1 : 0 }
        };

        dataObj[chrNum]["IMAGE_IMPORT_BY_NAME"] = tempObj;

        thunkData++;
    } while (thunkData->u1.AddressOfData != 0);

    return dataObj;
}

jsoncons::json 
SectionAnalysis::getImportDescriptor(
    PAlyStruct data
)
{
    DWORD64 addr = NULL;
    PIMAGE_IMPORT_DESCRIPTOR importDescriptor = data->importDescriptor;
    DWORD64 count = 0;
    CHAR chrNum[8] = { 0 };

    jsoncons::json dataObj;
    
    do {
        _itoa_s(count++, chrNum, 10);

        dataObj[chrNum] = jsoncons::json::object{
            { "OriginalFirstThunk",  importDescriptor->OriginalFirstThunk },
            { "FirstThunk",  importDescriptor->FirstThunk },
            { "Characteristics",  importDescriptor->Characteristics },
            { "ForwarderChain",  importDescriptor->ForwarderChain },
            { "Name",  (PCHAR)&data->baseAddr[SectionAnalysis::RVAtoFA(importDescriptor->Name, data)] },
            { "TimeDateStamp",  importDescriptor->TimeDateStamp },
        };

        addr = SectionAnalysis::RVAtoFA(importDescriptor->FirstThunk, data);
        dataObj[chrNum]["THUNK_DATA"] = data->arhitectura == 32 ? \
            getThunkData32((PIMAGE_THUNK_DATA32)&data->baseAddr[addr], data) : \
            getThunkData64((PIMAGE_THUNK_DATA64)&data->baseAddr[addr], data);

        importDescriptor++;
    } while (importDescriptor->Characteristics != 0);

    return dataObj;
}
