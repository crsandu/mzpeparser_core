#pragma once
#include <Windows.h>
#include "HeadersAnalysis.hpp"
#include "ParseFile.hpp"

class HeaderVerf {
public:
    static bool isValidMZPE(PAlyStruct data);
private:
    static bool isValidDosHeader(PAlyStruct data);
    static bool isValidNtHeader(PAlyStruct data);
    static bool isValidFileHeader(PAlyStruct data);
    static bool isValidOptionalHeader(PAlyStruct data);
    static bool isValidSectionHeaders(PAlyStruct data);
};