#include "ParseFile.hpp"

#include "HeaderVerf.hpp"
#include "HeadersAnalysis.hpp"

#include "SectionVerf.hpp"
#include "SectionAnalysis.hpp"

ParseFile::ParseFile(
    LPCWSTR fileName
)
/*
+++
# Parametrii:
    fileName = numele fisierului pe care dorim sa il parsam
*/
{
    enum{CreateFile_DONE, CreateFileMap_DONE, MapViewOfFile_DONE};
    DWORD state = 0;
    this->lastError = 0;

    this->fileHandle = CreateFileW(
        fileName,
        GENERIC_READ,
        FILE_SHARE_READ,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL

    );
    if (this->fileHandle == INVALID_HANDLE_VALUE) {
        goto cleanUp;
    }
    state = CreateFile_DONE;

    this->fileMap = CreateFileMappingA(
        this->fileHandle,
        NULL,
        PAGE_READONLY,
        0,
        0,
        NULL
    );
    if (this->fileMap == NULL) {
        goto cleanUp;
    }
    state = CreateFileMap_DONE;
    
    this->mapViewFile = (PBYTE)MapViewOfFile(
        this->fileMap,
        FILE_MAP_READ,
        0,
        0,
        0
    );
    if (mapViewFile == NULL) {
        std::cout << GetLastError();
        goto cleanUp;
    }
    state = CreateFileMap_DONE;

    this->fileHandle = fileHandle;
    this->fileMap = fileMap;
    this->mapViewFile = (PBYTE)mapViewFile;

    this->dataObjStr = new AlyStruct();
    this->dataObjStr->baseAddr = this->mapViewFile;
    this->dataObjStr->fileSize = this->getFileSize();

    return;

cleanUp:
    this->lastError = GetLastError();

    switch (state) {
    case MapViewOfFile_DONE:
        UnmapViewOfFile(mapViewFile);
    case CreateFileMap_DONE:
        CloseHandle(fileMap);
    case CreateFile_DONE:
        CloseHandle(fileHandle);
    default:
        this->fileHandle = NULL;
        this->fileMap = NULL;
        this->mapViewFile = NULL;
        this->dataObjStr = NULL;
    }
}

ParseFile::~ParseFile()
{
    if (this->mapViewFile) {
        UnmapViewOfFile(this->mapViewFile);
    }
    if (this->fileMap) {
        CloseHandle(this->fileMap);
    }
    if (this->fileHandle) {
        CloseHandle(this->fileHandle);
    }
}

DWORD64 ParseFile::getFileSize()
{
    DWORD highSizeOrder = 0;
    DWORD lowSizeOrder = 0;

    lowSizeOrder = GetFileSize(
        this->fileHandle,
        &highSizeOrder
    );

    return highSizeOrder << (sizeof(DWORD)) | lowSizeOrder;
}

std::string ParseFile::getStringFormat()
{
    std::string strJSON;
    jsoncons::json jData = jsoncons::json();

    jData["IMAGE_DOS_HEADER"] = HeaderAnalysis::getDosHeader(this->dataObjStr);

    if (this->dataObjStr->arhitectura == 32) {
        jData["IMAGE_NT_HEADERS"] = HeaderAnalysis::getNtHeader32(this->dataObjStr);
    }
    else {
        jData["IMAGE_NT_HEADERS"] = HeaderAnalysis::getNtHeader64(this->dataObjStr);
    }

    jData["IMAGE_SECTION_HEADER"] = HeaderAnalysis::getImageSection(this->dataObjStr);

    jData["IMAGE_EXPORT_DIRECTORY"] = SectionAnalysis::getExportDirectory(this->dataObjStr);
    jData["IMAGE_IMPORT_DESCRIPTOR"] = SectionAnalysis::getImportDescriptor(this->dataObjStr);

    jData.dump(strJSON);

    return strJSON;
}

bool ParseFile::verifyContent()
{
    return HeaderVerf::isValidMZPE(this->dataObjStr) && SectionVerf::isValidMZPE(this->dataObjStr);
}

