#include "SectionVerf.hpp"
#include "SectionAnalysis.hpp"

bool 
SectionVerf::isValidMZPE(
    PAlyStruct data
)
{
    DWORD64 addr = NULL;
    DWORD64 ImageBase = data->ImageBase;

    /* Data Directory */
    data->dataDirectory = data->arhitectura == 32 ? \
        data->imageOptionalHeader32->DataDirectory : \
        data->imageOptionalHeader64->DataDirectory;

    /* Export Directory */
    addr = SectionAnalysis::RVAtoFA(data->dataDirectory[0].VirtualAddress, data);
    data->exportDirectory = (PIMAGE_EXPORT_DIRECTORY)&data->baseAddr[addr];

    data->AddressOfFunctions    = (PDWORD)&data->baseAddr[SectionAnalysis::RVAtoFA(data->exportDirectory->AddressOfFunctions, data)];
    data->AddressOfNameOrdinals = (PWORD)&data->baseAddr[SectionAnalysis::RVAtoFA(data->exportDirectory->AddressOfNameOrdinals, data)];
    data->AddressOfNames        = (PDWORD)&data->baseAddr[SectionAnalysis::RVAtoFA(data->exportDirectory->AddressOfNames, data)];
    if (!isValidExportDirectory(data)) {
        return false;
    }

    /* Import Descriptor */
    addr = SectionAnalysis::RVAtoFA(data->dataDirectory[1].VirtualAddress, data);
    data->importDescriptor = (PIMAGE_IMPORT_DESCRIPTOR)&data->baseAddr[addr];
    if (!isValidImportDescriptor(data)) {
        return false;
    }

    return true;
}

bool 
SectionVerf::isValidExportDirectory(
    PAlyStruct data
)
{
    DWORD64 ImageBase = data->ImageBase;
    PIMAGE_EXPORT_DIRECTORY exportDirectory = data->exportDirectory;
    PWORD nameOrdinals = data->AddressOfNameOrdinals;
    PDWORD addressOfNames = data->AddressOfNames;
    PDWORD addresOfFunction = data->AddressOfFunctions;

    PCHAR addrName = nullptr;
    WORD ordinalValue = 0;
    DWORD64 addrFunction = 0;

    /* RVA to FA failed */
    if (exportDirectory == nullptr) {
        return false;
    }

    /* nu are functii exportate */
    if (exportDirectory->NumberOfFunctions == 0) {
        return true;
    }

    /* numarul de nume de functii este mai mare ca numarul de functii */
    if (exportDirectory->NumberOfFunctions < exportDirectory->NumberOfNames) {
        return false;
    }


    /* invalid data */
    if (!data->AddressOfFunctions || \
        !data->AddressOfNameOrdinals || \
        !data->AddressOfNames) {
        return false;
    }

    /* check the names of the export directory functions */
    for (DWORD32 i = 0; i < exportDirectory->NumberOfNames; i++) {
        addrName = (PCHAR)SectionAnalysis::RVAtoFA(addressOfNames[i], data);

        /* function is not in the file */
        if (addrName == nullptr) {
            return false;
        }
    }

    /* check the name ordinals */
    for (DWORD32 i = 0; i < exportDirectory->NumberOfFunctions; i++) {
        ordinalValue = data->AddressOfNameOrdinals[i];

        if (ordinalValue > exportDirectory->NumberOfNames) {
            return false;
        }
    }

    /* check the function addresses */
    for (DWORD i = 0; i < exportDirectory->NumberOfFunctions; i++) {
        addrFunction = SectionAnalysis::RVAtoFA(addresOfFunction[i], data);

        /* functia nu se afla in fisier */
        if (addrFunction == 0) {
            return false;
        }
    }
    return true;
}


bool
SectionVerf::isValidThunkData32(
    PIMAGE_THUNK_DATA32 thunkData,
    PAlyStruct data
) {
    DWORD64 addr = NULL;
    PIMAGE_IMPORT_BY_NAME imageData = nullptr;
    PIMAGE_THUNK_DATA32 tempThunk = thunkData;

    do {
        /* verificam daca THUNK_DATA-ul iese din fisier */
        if ((DWORD64)tempThunk - (DWORD64)data->baseAddr > data->fileSize) {
            return false;
        }

        /* import by name */
        if (!(tempThunk->u1.AddressOfData & (1 << 31))) {

            /* verificam daca adresa numelui functiei iese din fisiers*/
            addr = SectionAnalysis::RVAtoFA(tempThunk->u1.AddressOfData, data);
            if (addr == 0) {
                return false;
            }

            imageData = (PIMAGE_IMPORT_BY_NAME)&data->baseAddr[addr];
            for (int i = 0; i < data->fileSize - (DWORD64)&imageData->Name; i++) {
                if (imageData->Name[i] == '\0') {
                    return true;
                }
            }
            return false;
        }

        tempThunk++;
    } while (tempThunk->u1.AddressOfData != 0);
    return true;
}

bool
SectionVerf::isValidThunkData64(
    PIMAGE_THUNK_DATA64 thunkData,
    PAlyStruct data
) {
    DWORD64 addr = NULL;
    PIMAGE_IMPORT_BY_NAME imageData = nullptr;
    PIMAGE_THUNK_DATA64 tempThunk = thunkData;

    do {
        /* verificam daca THUNK_DATA-ul iese din fisier */
        if ((DWORD64)tempThunk - (DWORD64)data->baseAddr > data->fileSize) {
            return false;
        }

        /* import by name */
        if (!(tempThunk->u1.AddressOfData & ((DWORD64)1 << 63))) {

            /* verificam daca adresa numelui functiei iese din fisiers*/
            addr = SectionAnalysis::RVAtoFA(tempThunk->u1.AddressOfData, data);
            if (addr == 0) {
                return false;
            }

            imageData = (PIMAGE_IMPORT_BY_NAME)&data->baseAddr[addr];
            for (int i = 0; i < data->fileSize - (DWORD64)&imageData->Name; i++) {
                if (imageData->Name[i] == '\0') {
                    return true;
                }
            }
            return false;
        }

        tempThunk++;
    } while (tempThunk->u1.AddressOfData != 0);
    return true;
}

bool 
SectionVerf::isValidImportDescriptor(
    PAlyStruct data
)
/*
+++
# Detalii:
    Import Address Table (IAT): http://sandsprite.com/CodeStuff/Understanding_imports.html
    MSDN: https://docs.microsoft.com/en-us/windows/win32/debug/pe-format
    Documentatie MS: https://docs.microsoft.com/en-us/archive/msdn-magazine/2002/march/inside-windows-an-in-depth-look-into-the-win32-portable-executable-file-format-part-2
---
*/
{
    /* check all import descriptors */
    bool thunkCheck = false;
    DWORD64 addr = 0;
    PIMAGE_IMPORT_DESCRIPTOR importDescriptor = data->importDescriptor;
    PIMAGE_IMPORT_DESCRIPTOR tempDescriptor = importDescriptor;

    if (importDescriptor == NULL) {
        return false;
    }

    /* verificam datele despre IMPORT_DESCRIPTOR-ul */
    do {
        /* verificam daca IMPORT_DESCRIPTOR-ul iese din fisier */
        if ((DWORD64)tempDescriptor - (DWORD64)data->baseAddr > data->fileSize) {
            return false;
        }

        /* verificam datele din IMPORT_DESCRIPTOR */
        addr = SectionAnalysis::RVAtoFA(tempDescriptor->Name, data);
        if (addr == 0) {
            return false;
        }

        /* verificam datele despre IMAGE_TUNK */
        addr = SectionAnalysis::RVAtoFA(tempDescriptor->FirstThunk, data);
        if (addr == 0) {
            return false;
        }
        if (data->arhitectura == 32) {
            thunkCheck = SectionVerf::isValidThunkData32((PIMAGE_THUNK_DATA32)&data->baseAddr[addr], data);
        }
        else {
            thunkCheck = SectionVerf::isValidThunkData64((PIMAGE_THUNK_DATA64)&data->baseAddr[addr], data);
        }

        if (thunkCheck == false) {
            return false;
        }

        tempDescriptor++;
    } while (tempDescriptor->Characteristics != 0);
    return true;
}
