#pragma once
#include <Windows.h>
#include "jsoncons/json.hpp"

typedef struct _analysisStructure {
    DWORD64 fileSize;
    PBYTE baseAddr;

    /* structuri utilizate pentru accesarea mai rapida a datelor */
    PIMAGE_DOS_HEADER imageDosHeader;
    PIMAGE_FILE_HEADER imageFileHeader;

    PIMAGE_NT_HEADERS32 imageNtHeader32;
    PIMAGE_NT_HEADERS64 imageNtHeader64;

    PIMAGE_OPTIONAL_HEADER64 imageOptionalHeader64;
    PIMAGE_OPTIONAL_HEADER32 imageOptionalHeader32;

    PIMAGE_SECTION_HEADER imageSectionsHeaders;

    /* Section Headers */
    PIMAGE_DATA_DIRECTORY dataDirectory;
    PIMAGE_EXPORT_DIRECTORY exportDirectory;

    PIMAGE_IMPORT_DESCRIPTOR importDescriptor;

    /* date legate de program */
    BYTE arhitectura; // 64 or 32
    BOOL isDLL; // if the MZPE is dll file

    WORD NumberOfSections;
    DWORD FileAlignment;
    DWORD SectionAlignment;

    /* export directory */
    PDWORD AddressOfFunctions;
    PDWORD AddressOfNames;
    PWORD AddressOfNameOrdinals;

    DWORD64 ImageBase; // image base address (IMAGE_OPTIONAL_HEADER(32 / 64))
}AlyStruct, * PAlyStruct;

class ParseFile {
public:
    ParseFile(LPCWSTR fileName);
    DWORD64 getFileSize();
    std::string getStringFormat();
    bool verifyContent();
    ~ParseFile();

    DWORD lastError;
private:
    HANDLE fileHandle;
    HANDLE fileMap;
    PBYTE mapViewFile;

    PAlyStruct dataObjStr;
};
