﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MZPEParser_GUI
{
    class ProcessData
    {
        unsafe struct UNICODE_STRING64
        {
            public byte *Buffer;
            public int Length;
        };

        [DllImport("C:\\Users\\sandu_cristian\\source\\repos\\mzpeparser_core\\x64\\Debug\\MZPEParser_CORE.dll", CharSet = CharSet.Unicode)]
        static extern UNICODE_STRING64 parseMZ(char []fileName);

        private Dictionary<string, object> jsonMZPE = null;
        private bool validData;

        unsafe public ProcessData(string FileName)
        {
            UNICODE_STRING64 retData = parseMZ(FileName.ToCharArray());
            byte[] tempArray = new byte[retData.Length];

            /* datele sunt invalizi */
            if (retData.Length == 0)
            {
                this.validData = false;
                return;
            }


            Marshal.Copy(new IntPtr(retData.Buffer), tempArray, 0, retData.Length);
            jsonMZPE = JsonConvert.DeserializeObject<Dictionary<string, object>>(
                System.Text.Encoding.UTF8.GetString(tempArray)
            );

            this.validData = true;
        }

        public bool isValid()
        {
            return this.validData;
        }

        public Dictionary<string, object> getJson()
        {
            return this.jsonMZPE;
        }
    }
}
