﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MZPEParser_GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /* reference to all buttons */
        Button[] buttonsHeaders;

        OpenFileDialog fileDialog = new OpenFileDialog();
        string selectedFile = null;

        ProcessData pData;

        public MainWindow()
        {
            InitializeComponent();

            this.buttonsHeaders = new Button[] { this.buttonDOSHeader, this.buttonNTHeader, this.buttonFileHeader, this.buttonOptionalHeader, this.buttonSectionHeader, 
                           this.buttonExportDirectory, this.buttonExportFunction };
        }

        private void GetFilePath_Click(object sender, RoutedEventArgs e)
        {
            /* expunem file dialog-ul */
            this.fileDialog.ShowDialog();

            /* obtinem numele fisierului */
            this.selectedFile = this.fileDialog.FileName;
            this.FilePath.Text = this.selectedFile;

            pData = new ProcessData(this.selectedFile);

            if (!pData.isValid())
            {
                /* datele nu au fost valizi */
                this.FilePath.Text = "INVALID DATA";

                /* disable all header and directory buttons */
                foreach (Button btn in this.buttonsHeaders)
                {
                    btn.IsEnabled = false;
                }
            }
            else
            {
                /* enable all header and directory buttons */
                foreach (Button btn in this.buttonsHeaders)
                {
                    btn.IsEnabled = true;
                }

                Dictionary<string, object> jsonData = pData.getJson();

                Dictionary<string, object> exportDirectory = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData["IMAGE_EXPORT_DIRECTORY"].ToString());
                if(exportDirectory["NumberOfFunctions"].ToString() == "0")
                {
                    /* nu exista functii exportate */
                    this.buttonExportFunction.IsEnabled = false;
                }
            }
        }

        private DataTable generateTable(Dictionary<String, Object> dataDict)
        {
            DataTable dataTable = new DataTable();
            DataRow row = null;

            dataTable.Columns.Add("Member");
            dataTable.Columns.Add("Value");

            foreach (string memName in dataDict.Keys)
            {
                row = dataTable.NewRow();

                row["Member"] = memName;
                row["Value"] = dataDict[memName].ToString();

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }


        private DataTable generateTable(Dictionary<String, String> dataDict)
        {
            DataTable dataTable = new DataTable();
            DataRow row = null;

            dataTable.Columns.Add("Member");
            dataTable.Columns.Add("Value");

            foreach (string memName in dataDict.Keys)
            {
                row = dataTable.NewRow();

                row["Member"] = memName;
                row["Value"] = dataDict[memName];

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        private void buttonDOS_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> jsonData = pData.getJson();
            DataTable dataTable = null;
            Dictionary<string, string> dosHeader = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonData["IMAGE_DOS_HEADER"].ToString());

            dataTable = this.generateTable(dosHeader);

            ShowDataGrid.ItemsSource = dataTable.DefaultView;
        }

        private void buttonNTHeader_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> jsonData = pData.getJson();
            DataTable dataTable = new DataTable();
            Dictionary<string, object> ntHeaders = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData["IMAGE_NT_HEADERS"].ToString());
            DataRow row = null;

            dataTable.Columns.Add("Member");
            dataTable.Columns.Add("Value");

            row = dataTable.NewRow();

            row["Member"] = "Signature";
            row["Value"] = ntHeaders["Signature"].ToString();

            dataTable.Rows.Add(row);

            ShowDataGrid.ItemsSource = dataTable.DefaultView;
        }

        private void buttonFileHeader_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> jsonData = pData.getJson();
            DataTable dataTable = null;

            Dictionary<string, object> ntHeaders = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData["IMAGE_NT_HEADERS"].ToString());
            Dictionary<string, string> fileHeader = JsonConvert.DeserializeObject<Dictionary<string, string>>(ntHeaders["IMAGE_FILE_HEADER"].ToString());

            dataTable = this.generateTable(fileHeader);

            ShowDataGrid.ItemsSource = dataTable.DefaultView;
        }

        private void buttonOptionalHeader_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> jsonData = pData.getJson();
            DataTable dataTable = null;

            Dictionary<string, object> ntHeaders = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData["IMAGE_NT_HEADERS"].ToString());
            Dictionary<string, string> optionalHeader = JsonConvert.DeserializeObject<Dictionary<string, string>>(ntHeaders["IMAGE_OPTIONAL_HEADERS"].ToString());

            dataTable = this.generateTable(optionalHeader);

            ShowDataGrid.ItemsSource = dataTable.DefaultView;
        }

        private void buttonSectionHeader_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> jsonData = pData.getJson();
            DataTable dataTable = new DataTable();
            DataRow row = null;

            Dictionary<string, object> sectionHeaders = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData["IMAGE_SECTION_HEADER"].ToString());
            Dictionary<string, string> section;

            dataTable.Columns.Add("Name");
            dataTable.Columns.Add("VirtualSize");
            dataTable.Columns.Add("VirtualAddress");
            dataTable.Columns.Add("SizeOfRawData");
            dataTable.Columns.Add("PointerToRawData");
            dataTable.Columns.Add("PointerToRelocations");
            dataTable.Columns.Add("PointerToLinenumbers");
            dataTable.Columns.Add("NumberOfRelocations");
            dataTable.Columns.Add("NumberOfLinenumbers");
            dataTable.Columns.Add("Characteristics");

            foreach (string sectKey in sectionHeaders.Keys)
            {
                section = JsonConvert.DeserializeObject<Dictionary<string, string>>(sectionHeaders[sectKey].ToString());
                
                row = dataTable.NewRow();

                row["Name"] = section["Name"];
                row["VirtualSize"] = section["VirtualSize"];
                row["VirtualAddress"] = section["VirtualAddress"];
                row["SizeOfRawData"] = section["SizeOfRawData"];
                row["PointerToRawData"] = section["PointerToRawData"];
                row["PointerToRelocations"] = section["PointerToRelocations"];
                row["PointerToLinenumbers"] = section["PointerToLinenumbers"];
                row["NumberOfRelocations"] = section["NumberOfRelocations"];
                row["NumberOfLinenumbers"] = section["NumberOfLinenumbers"];
                row["Characteristics"] = section["Characteristics"];

                dataTable.Rows.Add(row);
            }


            ShowDataGrid.ItemsSource = dataTable.DefaultView;
        }

        private void buttonExportDirectory_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> jsonData = pData.getJson();
            DataTable dataTable = new DataTable();

            Dictionary<string, object> exportDirectory = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData["IMAGE_EXPORT_DIRECTORY"].ToString());
            exportDirectory.Remove("Address of Function");

            dataTable = this.generateTable(exportDirectory);
            ShowDataGrid.ItemsSource = dataTable.DefaultView;
        }

        private void buttonExportFunction_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> jsonData = pData.getJson();
            DataTable dataTable = new DataTable();
            DataRow row = null;

            Dictionary<string, object> exportDirectory = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData["IMAGE_EXPORT_DIRECTORY"].ToString());
            Dictionary<string, object> exportFunctions = JsonConvert.DeserializeObject<Dictionary<string, object>>(exportDirectory["Address of Function"].ToString());

            Dictionary<string, string> functionData;

            dataTable.Columns.Add("Ordinal");
            dataTable.Columns.Add("Name");
            dataTable.Columns.Add("RVA");

            foreach (string sectKey in exportFunctions.Keys)
            {
                functionData = JsonConvert.DeserializeObject<Dictionary<string, string>>(exportFunctions[sectKey].ToString());

                row = dataTable.NewRow();

                row["Ordinal"] = functionData["Ordinal"];
                row["Name"] = functionData["Name"];
                row["RVA"] = functionData["Address of function"];

                dataTable.Rows.Add(row);
            }


            ShowDataGrid.ItemsSource = dataTable.DefaultView;
        }

        private void ShowDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void buttonImportDescriptor_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> jsonData = pData.getJson();
            DataTable dataTable = new DataTable();
            DataRow row = null;

            Dictionary<string, object> importDescriptor = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData["IMAGE_IMPORT_DESCRIPTOR"].ToString());
            Dictionary<string, object> importData;

            dataTable.Columns.Add("OriginalFirstThunk");
            dataTable.Columns.Add("FirstThunk");
            dataTable.Columns.Add("Characteristics");
            dataTable.Columns.Add("ForwarderChain");
            dataTable.Columns.Add("Name");
            dataTable.Columns.Add("TimeDateStamp");

            foreach (string sectKey in importDescriptor.Keys)
            {
                importData = JsonConvert.DeserializeObject<Dictionary<string, object>>(importDescriptor[sectKey].ToString());
                row = dataTable.NewRow();

                row["OriginalFirstThunk"] = importData["OriginalFirstThunk"].ToString();
                row["FirstThunk"] = importData["FirstThunk"].ToString();
                row["Characteristics"] = importData["Characteristics"].ToString();
                row["ForwarderChain"] = importData["ForwarderChain"].ToString();
                row["Name"] = importData["Name"].ToString();
                row["TimeDateStamp"] = importData["TimeDateStamp"].ToString();

                dataTable.Rows.Add(row);
            }
            ShowDataGrid.ItemsSource = dataTable.DefaultView;
        }

        private void buttonThunkData_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> jsonData = pData.getJson();
            DataTable dataTable = new DataTable();
            DataRow row = null;

            Dictionary<string, object> importDescriptor = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData["IMAGE_IMPORT_DESCRIPTOR"].ToString());
            Dictionary<string, object> importThunk = JsonConvert.DeserializeObject<Dictionary<string, object>>(importDescriptor["IMAGE_IMPORT_DESCRIPTOR"].ToString());

            Dictionary<string, object> importData;

            dataTable.Columns.Add("OriginalFirstThunk");
            dataTable.Columns.Add("FirstThunk");
            dataTable.Columns.Add("Characteristics");
            dataTable.Columns.Add("ForwarderChain");
            dataTable.Columns.Add("Name");
            dataTable.Columns.Add("TimeDateStamp");

            foreach (string sectKey in importDescriptor.Keys)
            {
                importData = JsonConvert.DeserializeObject<Dictionary<string, object>>(importDescriptor[sectKey].ToString());
                row = dataTable.NewRow();

                row["OriginalFirstThunk"] = importData["OriginalFirstThunk"].ToString();
                row["FirstThunk"] = importData["FirstThunk"].ToString();
                row["Characteristics"] = importData["Characteristics"].ToString();
                row["ForwarderChain"] = importData["ForwarderChain"].ToString();
                row["Name"] = importData["Name"].ToString();
                row["TimeDateStamp"] = importData["TimeDateStamp"].ToString();

                dataTable.Rows.Add(row);
            }
            ShowDataGrid.ItemsSource = dataTable.DefaultView;
        }
    }
}
